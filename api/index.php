<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json; charset=utf-8');
require_once 'WeatherService.class.php';
require_once 'TransportService.class.php';

switch ($_GET['data']) {
  case 'weather':
    $weather = WeatherService::getWeather();
    die(json_encode($weather));
    break;
  case 'transport':
    $departures = @TransportService::getDepartures();
    die($departures);
  default:
    # code...
    break;
}
?>
