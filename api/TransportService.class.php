<?php

class TransportService {
  public static function getDepartures() {
    $fileUrl = 'http://www.dsb.dk/Rejseplan/bin/stboard.exe/mn?L=vs_liveticker&amp;ml=m&amp;&input=8600688!&boardType=dep&time=now&selectDate=today&productsFilter=111111111111&additionalTime=0&start=yes&outputMode=tickerDataOnly&maxJourneys=10';
    $data = str_replace('journeysObj = ', '', file_get_contents($fileUrl)); // remove javascript variable notation
    $data = str_replace("'", '"', $data); // replace single quotes with doublequotes so the data becomes valid JSON
    $data = json_decode($data, true); // json decode the data so we can manage it in php as an associative array
    unset($data['headTexts']); // remove various unnecessary properties
    unset($data['stationName']);
    unset($data['boardType']);
    unset($data['iconPath']);

    foreach($data as &$time) {
      // format the station names so that the html entitites become the native nordic letters, eg. R&#248dovre St becomes Rødvovre St
      $convertedString = html_entity_decode($time['st'], ENT_COMPAT, 'UTF-8');
      $time['st'] = $convertedString;
      $time['station'] = $time['st']; // the data comes with these weird property names, so st instead of station and ti instead of tid/time. We want our frontend code to be readable so we copy the value to a property name that makes sense and unset the old one, effectively renaming it
      unset($time['st']);

      if (strpos($time['pr'], 'Bus') !== false) {
        //if the bus/train name contains the word bus, we know it's a bus so we can assign a type and color for easy frontend purposes
        $time['type'] = 'bus';
        $time['color'] = '#FEC210';

        //we clean up the name by removing unnecessary whitespace and the word bus, so we can use the name directly in the icon on the frontend, and also set it to a property called name
        $time['name'] = trim(str_replace('Bus', '', $time['pr']));
        unset($time['pr']);
      } else {
        //if it's a train we give it a type of train and the color of the A train (that's the only train that passes emdrup st.)
        $time['type'] = 'train';
        $time['color'] = '#1EB4ED';

        // clean up and give it a sensible name
        $time['name'] = $time['pr'];
        unset($time['pr']);
        // die(json_encode($time));
      }
    }

    //we return the json
    return json_encode($data);
  }
}
