Date.prototype.getMonthName = function() {
    let month = new Array();
    month[0] = "Januar";
    month[1] = "Februar";
    month[2] = "Marts";
    month[3] = "April";
    month[4] = "Maj";
    month[5] = "Juni";
    month[6] = "Juli";
    month[7] = "August";
    month[8] = "September";
    month[9] = "Oktober";
    month[10] = "November";
    month[11] = "December";

    return month[this.getMonth()];
  }

  Date.prototype.getDayName = function() {
    let weekday = new Array(7);
    weekday[0] =  "Søndag";
    weekday[1] = "Mandag";
    weekday[2] = "Tirsdag";
    weekday[3] = "Onsdag";
    weekday[4] = "Torsdag";
    weekday[5] = "Fredag";
    weekday[6] = "Lørdag";
    // weekday[0] =  "Sunday";
    // weekday[1] = "Monday";
    // weekday[2] = "Tuesday";
    // weekday[3] = "Wednesday";
    // weekday[4] = "Thursday";
    // weekday[5] = "Friday";
    // weekday[6] = "Saturday";

    return weekday[this.getDay()];
  }

  Number.prototype.toDoubleDigit = function() {
      return this >= 10 ? this : '0' + this;
  }