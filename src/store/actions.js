let actions = {
    fetchDepartures(context) {
        fetch('/api?data=transport')
        .then(response => response.json())
        .then(departures => context.commit('updateDepartures', {departures}));
    },
    fetchEvents(context) {
        let events;
        const setup = {
            clientId: '276395112411-d32e3a8krajn45vmh8qnsqi61vrudvpl.apps.googleusercontent.com',
            scope: 'https://www.googleapis.com/auth/calendar.readonly',
            discoveryDocs: ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"]
        };
        function updateSigninStatus(isSignedIn) {
            if (isSignedIn) {
                console.log('is signed in');
                console.log(gapi.client.calendar);
                getEvents();
            } else {
              gapi.auth2.getAuthInstance().signIn();
            }
        }
        gapi.client.init(setup).then(() => {
            // Listen for sign-in state changes.
            gapi.auth2.getAuthInstance().isSignedIn.listen(() => updateSigninStatus);

            // Handle the initial sign-in state.
            updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
        });

        const options = {
          'calendarId': 'primary',
          'timeMin': (new Date()).toISOString(),
          // 'timeMax': (new Date()).toISOString(),
          'showDeleted': false,
          'singleEvents': true,
          'maxResults': 20,
          'orderBy': 'startTime'
        };

        function getEvents() {
            gapi.client.calendar.events.list(options).then(response => {
                //loop over events and mark them firstinmonth if they are
                let results = response.result.items;
                events = results.map((currentEvent, key, events) => {
                    currentEvent.index = key;
                    if (key == 0) return currentEvent;

                    let eventDateCurr = new Date(currentEvent.start.dateTime);
                    let eventDateCurrFormatted = eventDateCurr.getDay() + ',' + eventDateCurr.getMonth();

                    let eventDatePrev = new Date(events[key - 1].start.dateTime);
                    let eventDatePrevFormatted = eventDatePrev.getDay() + ',' + eventDatePrev.getMonth();

                    if (eventDateCurrFormatted !== eventDatePrevFormatted) {
                        currentEvent.firstInMonth = true;
                    }

                    return currentEvent;
                });
                context.commit('updateEvents', {events});
            });

        }

    }
}

export default actions;
