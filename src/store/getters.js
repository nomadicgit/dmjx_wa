
let getters = {
    eventById: (state) => (id) => {
        return state.events.filter(event => event.id == id)[0];
    }
}

export default getters;