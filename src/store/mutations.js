let mutations = {
    updateDepartures(state, payload) {
        console.log(payload);
        state.departures = payload.departures;
    },
    updateEvents(state, payload) {
        console.log(payload)
        state.events = payload.events;
    }
}

export default mutations;