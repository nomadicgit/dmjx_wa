import Schedule from './dashboard/Schedule.vue';
import EventDetails from './dashboard/EventDetails.vue';

const routes = [
  {
    component: Schedule,
    path: '/'
  },
  {
    component: EventDetails,
    path: '/event/:id',
    props: true
  }
]

export default routes;
